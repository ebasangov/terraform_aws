terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.21"
    }
  }
}
provider "aws" {
  profile = var.aws_profile # profile in ~/.aws/credentials
  region  = var.region
}
