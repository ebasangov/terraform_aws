# Resource: aws_subnet
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet
resource "aws_subnet" "public_1" {
  # The VPC ID.
  vpc_id = aws_vpc.main.id
  # The CIDR block for the subnet.
  cidr_block = var.subnet1_cidr_block
  # The AZ for the subnet.
  availability_zone = var.az1
  # Required for EKS. Instances launched into the subnet should be assigned a public IP address.
  map_public_ip_on_launch = true
  # A map of tags to assign to the resource.
  tags = {
    Name  = var.az1
    owner = var.owner_name
  }
}

resource "aws_subnet" "public_2" {
  # The VPC ID
  vpc_id = aws_vpc.main.id
  # The CIDR block for the subnet.
  cidr_block = var.subnet2_cidr_block
  # The AZ for the subnet.
  availability_zone = var.az2
  # Required for EKS. Instances launched into the subnet should be assigned a public IP address.
  map_public_ip_on_launch = true
  # A map of tags to assign to the resource.
  tags = {
    Name  = var.az2
    owner = var.owner_name
  }
}
