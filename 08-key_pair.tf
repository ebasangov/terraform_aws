resource "aws_key_pair" "my_aws_key_kode" {
  key_name   = "my_aws_key_kode"
  public_key = var.pub_key
}