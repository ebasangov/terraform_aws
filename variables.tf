variable "owner_name" {
  description = "The environment for the for aws home task and EKS cluster"
  default     = "user@example.com"
}

variable "key_name" {
  description = "Key name of the Key Pair to use for the instance"
  # type        = string
  default = "my_aws_key_kode"
}

variable "aws_profile" {
  description = "profile for aws cli in ~/.aws/credentials"
  default     = "kodekloud"
}
variable "env_name" {
  description = "The environment for the EKS cluster"
  default     = "prod"
}
variable "region" {
  description = "The region to host the cluster in"
  default     = "us-east-1"
}
variable "ami" {
  description = "Ubuntu 20.0 Free tier"
  default     = "ami-007855ac798b5175e"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "ip-range-pods"
}
variable "ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "ip-range-services"
}
variable "vpc_cidr_block" {
  description = "vpc cidr"
  default     = "192.168.0.0/16"
}

variable "subnet1_cidr_block" {
  description = "subnet public1 cidr"
  default     = "192.168.0.0/18"
}
variable "subnet2_cidr_block" {
  description = "subnet public cidr"
  default     = "192.168.64.0/18"
}

variable "az1" {
  description = "az1"
  default     = "us-east-1e"
}
variable "az2" {
  description = "az2"
  default     = "us-east-1f"
}


variable "db_username" {
}
variable "db_password" {
}
variable "pub_key" {
}
