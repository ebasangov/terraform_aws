resource "aws_route_table" "public" {
  # The VPC ID.
  vpc_id = aws_vpc.main.id

  route {
    # The CIDR block of the route.
    cidr_block = "0.0.0.0/0"

    # Identifier of a VPC internet gateway or a virtual private gateway.
    gateway_id = aws_internet_gateway.main-gw.id
  }

  # A map of tags to assign to the resource.
  tags = {
    Name  = "public"
    owner = var.owner_name
  }
}
