resource "aws_instance" "web_server_a" {
  ami             = var.ami
  instance_type   = var.instance_type
  subnet_id       = aws_subnet.public_1.id
  key_name        = var.key_name
  security_groups = [aws_security_group.web.id, aws_security_group.efs.id, aws_security_group.db.id]

  user_data = file("user-data-apache.sh")
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name  = "web_server_eu-central-1a"
    owner = var.owner_name
  }
}

resource "aws_instance" "web_server_b" {
  ami             = var.ami
  instance_type   = var.instance_type
  subnet_id       = aws_subnet.public_2.id
  key_name        = var.key_name
  security_groups = [aws_security_group.web.id, aws_security_group.efs.id, aws_security_group.db.id]

  user_data = file("user-data-apache.sh")

  tags = {
    Name  = "web_server_eu-central-1b"
    owner = var.owner_name

  }
}