# Resource: aws_vpc
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
resource "aws_vpc" "main" {
  # The CIDR block for the VPC.
  cidr_block = var.vpc_cidr_block
  # Makes your instances shared on the host.
  instance_tenancy = "default"
  # Required for EKS. Enable/disable DNS support in the VPC.
  enable_dns_support = true
  # Required for EKS. Enable/disable DNS hostnames in the VPC.
  enable_dns_hostnames = true
  # Enable/disable ClassicLink for the VPC.
  enable_classiclink = false
  # Enable/disable ClassicLink DNS Support for the VPC.
  enable_classiclink_dns_support = false
  # ipv6_cidr_block
  assign_generated_ipv6_cidr_block = false
  # tags to assign to the resource.
  tags = {
    Name  = "main"
    owner = var.owner_name
  }
}
output "vpc_id" {
  value       = aws_vpc.main.id
  description = "VPC id."
  sensitive   = false
}